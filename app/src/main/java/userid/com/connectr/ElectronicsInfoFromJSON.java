package userid.com.connectr;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by venkat on 7/26/2015.
 */
public class ElectronicsInfoFromJSON {
    public static String ELECTRONICSJSON_API = "http://api.walmartlabs.com/v1/paginated/items?format=json&category=3944&apiKey=seuyq22f8ejzzvq4fdxuh3zd";

    List<Map<String,?>> electronicsList;

    public void dowloadElectronicsinfoJSON(String ELECTRONICSJSON_API){
        electronicsList.clear();

        String electronicsArray = MyUtility.downloadJSON(ELECTRONICSJSON_API);
        if(electronicsArray != null){
            Log.d("MyDebugMsg","Having trouble loading URL: " + ELECTRONICSJSON_API);
            return;
        }

        try{
            JSONArray electronicsJsonArray = new JSONArray(electronicsArray);
            for(int i=0; i< 25; i++){
                JSONObject eitemJSONObject = (JSONObject)electronicsJsonArray.get(i);
                if(eitemJSONObject != null){
                    String name = (String)eitemJSONObject.get("name");
                    int price = (Integer)eitemJSONObject.get("salePrice");
                    String description = (String)eitemJSONObject.get("longDescription");
                    String brandname = (String)eitemJSONObject.get("brandName");
                    String tnailimg = (String) eitemJSONObject.get("thumbnailImage");
                    String mediumimg = (String)eitemJSONObject.get("mediumImage");
                    String limg = (String)eitemJSONObject.get("largeImage");
                    String standshiprate = (String)eitemJSONObject.get("standardShipRate");
                    String marketPL = (String)eitemJSONObject.get("marketplace");
                    String stock = (String)eitemJSONObject.get("stock");
                    electronicsList.add(createanElectronicItem(name,price,description,brandname,tnailimg,mediumimg,
                            limg,standshiprate,marketPL,stock));
                }
            }
        }
        catch (JSONException ex){
            Log.d("MyDebugMsg", "JSONException in download of eJSONArray");
            ex.printStackTrace();
        }
    }

    private static HashMap createanElectronicItem (String name, int price, String description, String brandname, String tnailimg,
                                                   String mediumimg, String limg, String standshiprate, String marketPL, String stock)
    {
        HashMap eitem = new HashMap();
        eitem.put("name", name);
        eitem.put("price", price);
        eitem.put("description", description);
        eitem.put("brandname", brandname);
        eitem.put("tnailimg", tnailimg);
        eitem.put("mediumimg", mediumimg);
        eitem.put("limg", limg);
        eitem.put("standshiprate", standshiprate);
        eitem.put("marketPL", marketPL);
        eitem.put("stock",stock);

        return eitem;
    }
}
